/*
 * bird.h
 *
 *  Created on: 2 May 2013
 *      Author: Robel
 */
/*
#ifndef BIRD_H_
#define BIRD_H_
*/
#define PERIOD_LEADER 100  //period of the leader thread
#define PERIOD_FOLLOWER 100 //period of the follower threads(N-1) threads
#define EPSILON 0.05   //used for floating point comparison( wont be needed)
#define PERIOD_MAIN 50 //period of the main thread
#define MAX_BIRDS 10  //maximum number of birds
#define BIRD_COLOR(x) makecol(30*x,x*10,10+20*x)  //MACRO to generate bird colors
#define LEADER 0  //bird type leader ID
#define FOLLOWER 1  // bird type FOLLOWER ID
#define MOVE_SPEED 0.05  // speed for followers(equal to X% movement in leader direction) 
#define LEADER_SPEED 5 // speed of the leader(5 pixels towards a pseudo-random direction)
#define ALLIGN_SPEED 0.005 //allignment speed of the follower birds(as a % towards the flock centre)
#define SEPARATE_SPEED 7    //separation speed to avoid a potential collision
#define SEPARATION_RADIUS 20 //radius of separation between birds in pixels
#define FLOCK_RADIUS 40 //radius of a flock in pixels
#define x 0 
#define y 1 
#define BIRD_RADIUS 9  // bird radius
#define Y_LEN 600  // screen length
#define X_LEN 800 //screen width
#define NUM_HIST 3
#define LEFT 0
#define RIGHT 1
#define UP 2
#define DOWN 3
 
 /*
  *structure to represent a bird
  */
 typedef struct 
 {
  int type; // bird type leader or follower
  int id; // bird ID = 0: N-1
  float currentPos[2]; // current position of the bird 
  float positionHistory[2]; //last NUM_HIST positions of the leader
  float foward[NUM_HIST][2];
  int blocked;
  int ismove;
 }BIRD;
 
 
 /**
  * possible directions  a leader may take
  */
 typedef enum 
 {
   NORTH,SOUTH,EAST,WEST,NE,NW,SE,SW,NUM_DIR
 }DIRECTION;
 
 extern BIRD **birds; //stores bird details
 extern int numBirds; //stores number of birds
extern int index_leader;
 
 void set_leader_dir(BIRD *b,float *pos);
 /**
  * 
  */
 void init_simulation(int num);
 
 /**
  * 
  */
 BIRD* add_bird(int type);
 
 /**
  * 
  */
 void set_start_pos(BIRD *b);
 
 /**
  * 
  */
 void move_bird(BIRD *b);
 
 /**
  * 
  */
 void allign_bird(BIRD *b);
 
 /**
  * 
  */
 void separate_bird(BIRD *b);
 
 /**
  * 
  */
 int check_collision(BIRD *b);
 
 /**
  * 
  */
float get_separation(BIRD *b1,BIRD *b2);

 /**
  * 
  */
void calculate_center(BIRD *b,float *center);

 /**
  * 
  */
 void read_sensor(BIRD *b);
 
 /**
  *test functions
  */
 void print_positions(int i);
 DIRECTION get_leader_direction(BIRD *b);
//#endif /* BIRD_H_ */
