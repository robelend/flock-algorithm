all: main.o graphics.o bird.o
	gcc main.c graphics.c bird.c -L /usr/local/lib -lalleg -lm -lXxf86vm -lXcursor -lXpm -lX11 -lpthread -ldl -o bird -lrt

main.o: graphics.o bird.o
	gcc -c main.c
graphics.o: bird.o graphics.h
	gcc -c graphics.c
bird.o: bird.h
	gcc -c bird.c
clean:
	rm -rf *o all