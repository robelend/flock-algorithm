/*
 * graphics.h
 *
 *  Created on: 5 May 2013
 *      Author: Robel
 */

#ifndef GRAPHICS_H_
#define GRAPHICS_H_
#include "allegro.h"
#include "bird.h"
#include<stdlib.h>
#include<stdio.h>


void draw_birds();
void draw_bird(BIRD *b);


#endif /* GRAPHICS_H_ */
