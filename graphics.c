/*
 * graphics.c
 *
 *  Created on: 5 May 2013
 *      Author: Robel
 */

#include "graphics.h"


void draw_birds()
{
int i=0;
for(i=0;i<numBirds;i++)
	draw_bird(birds[i]);
}
void draw_bird(BIRD *b)
{

  float bx=b->currentPos[x];
  float by=b->currentPos[y];
  
  if(bx<=0)
  {
    while((bx=X_LEN+bx)<0);
    
  }
  
  if(bx>X_LEN)
  {
     while((bx=bx-X_LEN)>X_LEN);
  }
  if(by<=0)
  {
    while((by=Y_LEN+by)<0);
    
  }
  if(by>Y_LEN)
  {
     while((by=by-Y_LEN)>Y_LEN);
  }
  
  if(b->type==LEADER)
    circlefill(screen,bx,by,BIRD_RADIUS,makecol(255,0,0));
  else
    circlefill(screen,bx,by,BIRD_RADIUS,BIRD_COLOR(b->id));
    
}

