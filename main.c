/*
 * main.c
 *
 *  Created on: 15 May 2013
 *      Author: Robel
 */
#include "graphics.h"
#include<pthread.h>
#include<time.h>
#include<semaphore.h>

sem_t mutex;

void time_add_ms(struct timespec *t, int ms) {
	t->tv_sec += ms / 1000;
	t->tv_nsec += (ms % 1000) * 1000000;
	if (t->tv_nsec > 1000000000) {
		t->tv_nsec -= 1000000000;
		t->tv_sec += 1;
	}
}

void *fly_follower(void *p) {
	BIRD *b = (BIRD *) (p);
	struct timespec t;
	int period = PERIOD_FOLLOWER;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_add_ms(&t, period);

	while (1) {
	   // printf("follower %d\n",b->id);
		sem_wait(&mutex); //to protect the critical region
		allign_bird(b);
		move_bird(b);
		separate_bird(b);
		
		sem_post(&mutex);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL );
		time_add_ms(&t, period);
	}
}

void *fly_leader(void *p) {
	BIRD *b = (BIRD *) (p);
	struct timespec t;
	int period = PERIOD_LEADER;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_add_ms(&t, period);

	while (1) {
	   // printf("leader %d\n",b->id);
		sem_wait(&mutex);
		move_bird(b);
		//separate_bird(b);
		//allign_bird(b);
		sem_post(&mutex);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL );
		time_add_ms(&t, period);
	}
}

int main(int argc, char **argv) {
	int num;
	
	if (argc > 1) {
		if (sscanf(argv[1], "%d", &num) <= 0) {
			fprintf(stderr, "usage: wrong input parameters supplied\n");
			fprintf(stdout, "usage: wrong input parameters supplied\n");
			return -1;
		}
	} else {
		fprintf(stderr, "usage:no input parameters supplied\n");
		fprintf(stdout, "usage: no input parameters supplied\n");
		return -1;
	}

	init_simulation(num);
	sem_init(&mutex, 0, 1);
	int period = PERIOD_MAIN;
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_add_ms(&t, period);
	pthread_t leader;
	pthread_t *follower = (pthread_t*) malloc(sizeof(pthread_t) * (num - 1));

	allegro_init();
	install_keyboard();
	set_window_title("Bird Formation Simulator");
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, X_LEN, Y_LEN, 0, 0);
	set_palette(desktop_palette);

	pthread_create(&leader, NULL, fly_leader,(void *) birds[0]);
	int i = 1;
	for (i = 1; i < numBirds; i++)
		pthread_create(&follower[i], NULL, fly_follower, (void *)birds[i]);

	while (!key[KEY_ESC]) {
		acquire_screen();
		clear_to_color(screen, makecol(255, 255, 255));

		sem_post(&mutex);//mutual exclusion semaphore to protect birds array
		draw_birds();
		sem_post(&mutex);

		release_screen(); 
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL );
		time_add_ms(&t, period);
	}
	return 0;
}
