/*
 * bird.c
 *
 *  Created on: 5 May 2013
 *      Author: Robel
 */
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include "bird.h"

BIRD **birds;
int numBirds;
int isBegin=1;
void init_simulation(int num) {
	if (!num)
		return;
	birds = (BIRD**) malloc(sizeof(BIRD) * num);
	numBirds = num;
	int i = 0;
	birds[i] = add_bird(LEADER);
	birds[i]->id=0;
	birds[i]->type=LEADER;
	set_start_pos(birds[i]);
	for (i = 1; i < num; i++) {
		birds[i] = add_bird(FOLLOWER);
		birds[i]->id=i;
		birds[i]->type=FOLLOWER;
		set_start_pos(birds[i]);
	}
}
BIRD* add_bird(int type) {
	BIRD *b = (BIRD *) malloc(sizeof(BIRD));
	if (b == NULL )
		return NULL ; 
	return b;
}
void set_start_pos(BIRD *b) {
  
  if(b->type==LEADER)
  {
    b->currentPos[x]=X_LEN*0.5;
    b->currentPos[y]=Y_LEN*0.5;
    b->positionHistory[x]=X_LEN*0.5;
    b->positionHistory[y]=Y_LEN*0.5;
    printf("start pos leader %f %f \n", b->currentPos[x], b->currentPos[y]);
    return;
  }
 /*TODO: set start position of followers*/
 unsigned int iseed = (unsigned int) time(NULL );
	srand(iseed);
	unsigned int th = (int) (30.0 * rand() / (RAND_MAX + 1.0));
	th+=SEPARATION_RADIUS;
if(b->id%2){
 b->currentPos[x]=X_LEN*0.5+b->id*th+30;
 b->currentPos[y]=Y_LEN*0.5+b->id*th;
}
else
{
   b->currentPos[x]=X_LEN*0.5+b->id*th;
 b->currentPos[y]=Y_LEN*0.5-b->id*th;
}
 printf("start pos %f %f \n", b->currentPos[x], b->currentPos[y]);
 
 return;
}

int check_collision(BIRD *b)
{
  int i;
    for(i=0;i<numBirds;i++)
    {
      if(birds[i]->id!=b->id){
	  if(get_separation(birds[i],b)<SEPARATION_RADIUS*2)
	    return 1;      
      }
    }
   
   /*if((b->currentPos[x]-SEPARATION_RADIUS)<=0 ||(b->currentPos[x]+SEPARATION_RADIUS)>X_LEN)
      return 1;
    if((b->currentPos[y]-SEPARATION_RADIUS)<=0 ||(b->currentPos[y]+SEPARATION_RADIUS)>Y_LEN)
      return 1;
    */
      return 0;
}

float get_separation(BIRD *b1,BIRD *b2)
{
  return sqrt(pow(b1->currentPos[x]-b2->currentPos[x],2)+pow(b1->currentPos[y]-b2->currentPos[y],2));
  
}

void calculate_center(BIRD *b,float *center)
{
  float _x,_y;
  unsigned int i=0, num=0;
  
  _x=b->currentPos[x];
  _y=b->currentPos[y];
  
  for(i=0;i<numBirds;i++)
  {
    if(abs(birds[i]->currentPos[x]-b->currentPos[x])>=FLOCK_RADIUS && abs(birds[i]->currentPos[y]-b->currentPos[y])>=FLOCK_RADIUS)
    {
      _x+=birds[i]->currentPos[x];
      _y+=birds[i]->currentPos[y];
      num++;
      printf("bird id %d flock %d\n",b->id,birds[i]->id);//getchar();
    }
    
  }
  if(!num)return;
  center[x]=_x/num;
  center[y]=_y/num;
  return;
}
 void move_bird(BIRD *b)
 {
   if(b->type==FOLLOWER){
   //float leader[2];
   float v=birds[0]->currentPos[x];
   float w=birds[0]->currentPos[y];
   float _x=b->currentPos[x];
   float _y=b->currentPos[y];
   float dx,dy;
   if(_x>v)
      dx=(-1)*(_x-v)*MOVE_SPEED;
   else if(v>_x)
      dx=(v-_x)*MOVE_SPEED;
   else
      dx=v;
   if(_y>w)
      dy=(-1)*(_y-w)*MOVE_SPEED;
   else if(w>_y)
      dy=(w-_y)*MOVE_SPEED;
   else
      dy=w;
   b->currentPos[x]+=dx;
   b->currentPos[y]+=dy;
    if(check_collision(b)) 
   {
    printf("collision\n");
    b->currentPos[x]-=dx;
   b->currentPos[y]-=dy;
   
   }
   //print_positions(b->id);
//getchar();
   return;
   }
   /*leader movements*/
   
   unsigned int iseed = (unsigned int) time(NULL );
   srand(iseed);
   float dx,dy;
   if(isBegin)
   {
     isBegin=0;
  
     dx-=(float)(-1)*LEADER_SPEED*(rand()/(RAND_MAX+1.0));
     
     if((float)(rand()/(RAND_MAX+1.0))>0.5)
       dy=-1;
     else 
       dy=1;
     dy*=LEADER_SPEED*(float)(rand()/(RAND_MAX+1.0));
      
    //printf("myrand: %f %f\n",dy,dx);getchar();
   b->positionHistory[x]=b->currentPos[x];
   b->positionHistory[y]=b->currentPos[y];
   b->currentPos[x]+=dx;
   b->currentPos[y]+=dy; 
   //return;
   }
   
   if((float)(rand()/(RAND_MAX+1.0))>0.5)
     dx=-1;
   else
     dx=1;
    srand((unsigned int) time(NULL ));
   if((float)(rand()/(RAND_MAX+1.0))<0.5)
     dy=-1;
   else
     dy=1;
   
   dx*=LEADER_SPEED*(float)(rand()/(RAND_MAX+1.0));
   dy*=(float)(rand()/(RAND_MAX+1.0))*LEADER_SPEED;
   
   float px=b->positionHistory[x],py=b->positionHistory[y];
   
   b->positionHistory[x]=b->currentPos[x];
   b->positionHistory[y]=b->currentPos[y];
   /*
   if(px<b->currentPos[x]){
     if(dx<0 )dx*=-1;
     float cx=b->currentPos[x];
      while(b->currentPos[x]<=cx)
	 b->currentPos[x]+=dx;
   }
   else
   {
     if(dx>0)dx*=-1;
     float cx=b->currentPos[x];
      while(b->currentPos[x]>=cx)
	 b->currentPos[x]+=dx;
   }
   
    if(py<b->currentPos[y]){
     if(dy<0)dy*=-1;
     float cy=b->currentPos[y];
      while(b->currentPos[y]<=cy)
	 b->currentPos[y]+=dy;
   }
   else
   {
     if(dy>0)dy*=-1;
     float cy=b->currentPos[y];
      while(b->currentPos[y]>=cy)
	 b->currentPos[y]+=dy;
   }
   
   */
      b->currentPos[x]+=dx;
   b->currentPos[y]+=dy;
  while(check_collision(b)) 
   {
    printf("collision leader\n");
     srand((unsigned int) time(NULL ));
   if((float)(rand()/(RAND_MAX+1.0))>0.5)
     dx=-1;
   else
     dx=1;
   
   if((float)(rand()/(RAND_MAX+1.0))<0.5)
     dy=-1;
   else
     dy=1;
    b->currentPos[x]+=dx;
   b->currentPos[y]+=dy;
   } 
  
   print_positions(b->id);
  // getchar();
 }
 void allign_bird(BIRD *b)
 {
   float center[2];
   center[x]=0.0;
   center[y]=0.0;
   calculate_center(b,center);
   if(center[x]==0.0)return;
   float v=center[x];
   float w=center[y];
   float _x=b->currentPos[x];
   float _y=b->currentPos[y];
    float dx,dy;
   if(_x>v)
      dx=(-1)*(_x-v)*ALLIGN_SPEED;
   else if(v>_x)
      dx=(v-_x)*ALLIGN_SPEED;
   else
      dx=v;
   if(_y>w)
      dy=(-1)*(_y-w)*ALLIGN_SPEED;
   else if(w>_y)
      dy=(w-_y)*ALLIGN_SPEED;
   else
      dy=w;
   b->currentPos[x]+=dx;
   b->currentPos[y]+=dy;
   if(check_collision(b)) 
   {
     b->currentPos[x]-=dx;
   b->currentPos[y]-=dy;
   }
 if(b->currentPos[x]!=b->currentPos[x])
 {printf("NaN detected!!!\n");
   }
///getchar();
   
 }
 void separate_bird(BIRD *b)
 {
   float center[2];
  // calculate_center(b,center);
   
 }
void read_sensor(BIRD *b)
{
  
}

void print_positions(int i)
{
 // int i;
  //for(i=0;i<numBirds;i++)
    printf("id %d position (%f,%f)\n",birds[i]->id,birds[i]->currentPos[x],birds[i]->currentPos[y]);
}
